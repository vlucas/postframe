'use strict';

// Express
var express = require('express');
var router = express.Router();

/**
 * Configure Express
 */
var app = express();

// Static files
app.use('/src', express.static(__dirname + '/src'));
app.use('/', express.static(__dirname + '/test'));

/**
 * Start Server
 */
var port = process.env.PORT || 8765;
var server = app.listen(port, function() {
    console.log('Express server started at http://localhost:%d', server.address().port);
});
