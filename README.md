# Postframe.js

A wrapper around the
[postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
API that facilitates easier bi-directional communication between pages and the
iframes embeded in them, or the popups they open.

## Viewing The Example

The examples are in the `examples` folder, and are static HTML files
(index.html and iframe.html). A super simple node app is included in order to
run the demo on http://localhost:8001 instead of in a file:// path (which will
not work).

To get the node app up and running to view the examples, just run:

```
cd examples/iframe
npm install
node app.js
```

Then, navigate to [http://localhost:8001](http://localhost:8001) in your browser.

## Usage Instructions

Postframe.js is a small JavaScript file that is included on both the source and
destination page to facility easy cross-site communication through iframes and
external windows via the [postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage) API.

### How Postframe works:

1. Include postframe.js on your page
2. Send data to the target window or iframe with `postframe.send(window, 'someCommand', callback)`
3. Include postframe.js on the target iframe or popup window
4. Register an event handler for `someCommand` with `postframe.on('someCommand', callback)`
5. When your message is sent from your main page in step (2), the event listener you create on the iframe or popup window in step (4) will be executed in the iframe or popup window, and will then will send a message back to the original/parent window with the result as a parameter in the callback.

If the target iframe or popup window does NOT have an event listener for the data or command you send (in this case `someCommand`), nothing will happen.

### Example Code
On `index.html` with the `iframe` embedded (or where the external window is opened):

```javascript
<script src="postframe.js"></script>
<script>
  var iframe = document.getElementById('iframe');
  var postframe = new Postframe('http://' + location.host);

  // Send 'getContent' message to iframe, then do something with results
  postframe.send(iframe.contentWindow, 'getContent', function(result) {
      alert("I got the page content!\n\n" + result);
  });
</script>
```

On `iframe.html` or external popup window:

```javascript
<script src="postframe.js"></script>
<script>
  var postframe = new Postframe('http://' + location.host);

  // Listen for 'getContent' event, and call done() with results
  postframe.on('getContent', function(done) {
    done(document.body.innerHTML);
  });
</script>
```

## Running Tests

Postframe.js comes with a test suite. Due to the fact that Postframe.js works
with iframes and external windows that must exist in the DOM, **the tests ahve
to be run in a browser**.

To get the test server up and running:

```
npm install
node test-server.js
```

Then, visit [http://localhost:8765/](http://localhost:8765/) in your browser.
The tests should automatically run (and all pass, of course!).

The test suite is powered by [Mocha.js](http://mochajs.org/) and
[Chai.js](http://chaijs.com/).

