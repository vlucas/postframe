var expect = chai.expect;

describe('Postmessage iframe', function () {

  // Setup Postframe on this page
  var iframe;
  var postframe = new Postframe('http://' + location.host);

  // Add <iframe> dynamically so host/port are not hard-coded into HTML
  before(function(done) {
    iframe = document.createElement('iframe');
    iframe.id = 'iframe';
    iframe.src = 'http://' + location.host + '/iframe.html';
    iframe.addEventListener('load', function () { done(); });
    document.getElementsByTagName('body')[0].appendChild(iframe);
  });

  describe("send()", function () {
    it('should get content inside iframe', function (done) {
      postframe.send(iframe.contentWindow, 'getContent', function (result) {
        expect(result).to.be.a('string');
        expect(result).to.not.equal('');
        done();
      });
    });

    it('should cleanup callbacks after executing them', function (done) {
      var test1 = false;
      var test2 = false;
      var cbCount = 0;

      postframe.send(iframe.contentWindow, 'getContent', function (result) {
        expect(result).to.be.a('string');
        test1 = true;
        cbCount++;
        checkDone();
      });

      postframe.send(iframe.contentWindow, 'getContent', function (result) {
        expect(result).to.be.a('string');
        test2 = true;
        cbCount++;
        checkDone();
      });

      function checkDone () {
        if (test1 && test2) {
          expect(cbCount).to.equal(2);
          done();
        }
      }
    });

    it('should allow multiple events to attach and execute', function (done) {
      var test1 = false;
      var test2 = false;

      postframe.send(iframe.contentWindow, 'getContent', function (result) {
        expect(result).to.be.a('string');
        test1 = true;
        checkDone();
      });

      postframe.send(iframe.contentWindow, 'getContent', function (result) {
        expect(result).to.be.a('string');
        test2 = true;
        checkDone();
      });

      function checkDone () {
        if (test1 && test2) {
          done();
        }
      }
    });

    it('should execute callback with correct parameters with string', function (done) {
      postframe.send(iframe.contentWindow, 'withParams', 'param1', function (result, param) {
        expect(result).to.equal('done1');
        expect(param).to.equal('param1');
        done();
      });
    });

    it('should execute two callbacks with correct parameters', function (done) {
      postframe.send(iframe.contentWindow, 'withParams', { param2: true }, function (result, param) {
        expect(param).to.deep.equal({ param2: true });
        done();
      });
    });

    it('should execute callback with correct parameters', function (done) {
      var test1 = false;
      var test2 = false;

      postframe.send(iframe.contentWindow, 'withParams', 'param1', function (result, param) {
        expect(result).to.equal('done1');
        expect(param).to.equal('param1');
        test1 = true;
        checkDone();
      });

      postframe.send(iframe.contentWindow, 'withParams', { param2: true }, function (result, param) {
        expect(result).to.equal('done2');
        expect(param).to.deep.equal({ param2: true });
        test2 = true;
        checkDone();
      });

      function checkDone () {
        if (test1 && test2) {
          done();
        }
      }
    });

    it('should send command without callback', function () {
      postframe.send(iframe.contentWindow, 'noCallback');
    });

    it('should send command without callback but with data', function (done) {
      postframe.send(iframe.contentWindow, 'noCallbackWithData', 'New Frame Title');
      setTimeout(function() {
        expect(iframe.contentWindow.document.title).to.equal('New Frame Title');
        done();
      }, 100);
    });
  });

  describe('on()', function() {
    it('should fire multiple event callbacks when multiple listeners are attached', function (done) {
      postframe.send(iframe.contentWindow, 'multiple', function(result) {
        expect(iframe.contentWindow.document.title).to.contain(result);
        expect(iframe.contentWindow.document.title).to.contain('(1)');
        expect(iframe.contentWindow.document.title).to.contain('(2)');
        done();
      });
    });

    it('should receive the event object as the first parameter', function (done) {
      postframe.send(iframe.contentWindow, 'eventResult', function(result) {
        expect(result).to.equal(true)
        done();
      });
    });
  });

  describe('debugFn()', function() {
    it('should allow custom debug functions', function (done) {
      var msgs = [];
      postframe.debugFn(function (msg) {
        msgs.push(arguments);
      });
      postframe.send(iframe.contentWindow, 'eventResult', function(result) {
        done();
      });

      console.log(msgs);
      expect(msgs.length).to.equal(3);
    });
  });
});
