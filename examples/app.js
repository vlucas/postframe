'use strict';

// Express
var express = require('express');
var router = express.Router();

/**
 * Configure Express
 */
var app = express();
app.use('/src', express.static(__dirname + '../../src'));
app.use(express.static(__dirname + '/'));

/**
 * Start Server
 */
var port = process.env.PORT || 8001;
var server = app.listen(port, function() {
    console.log('Express server started at http://localhost:%d', server.address().port);
});
